package iftm.sextoperiodo.teds.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import iftm.sextoperiodo.teds.been.Cliente;
import iftm.sextoperiodo.teds.been.Resposta;
import iftm.sextoperiodo.teds.controller.ClienteController;

@WebServlet("/loadcliente")
public class LoadCliente extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		ClienteController cc = new ClienteController();
		Resposta res = new Resposta();
		
		List<Cliente> clientes = null;
		
		try {
			
			clientes = cc.retornaClientes();
			res.setData(clientes);
			
		} catch (Exception e) {
			res.setMsg(e.getMessage());
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		resp.setContentType("application/json");
		PrintWriter out = resp.getWriter();
		out.println(gson.toJson(res, Resposta.class));

	}

}
