package iftm.sextoperiodo.teds.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import iftm.sextoperiodo.teds.been.Resposta;
import iftm.sextoperiodo.teds.controller.ClienteController;


@WebServlet("/addcliente")
public class AddCliente extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		StringBuilder sb = new StringBuilder();
		String s = null;;
		while ((s = req.getReader().readLine()) != null) {
			sb.append(s);
		}
		
		ClienteController cc = new ClienteController();
		Resposta res = new Resposta();
		
		try {
			cc.registraCliente(sb.toString());
			res.setMsg("Cliente registrado com sucesso!");
			
		} catch (Exception e) {
			res.setMsg(e.getMessage());
		}

		Gson gson = new Gson();
		resp.setContentType("application/json");
		PrintWriter out = resp.getWriter();
		out.println(gson.toJson(res, Resposta.class));

	}

}
