package iftm.sextoperiodo.teds.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import iftm.sextoperiodo.teds.controller.ClienteController;

/**
 * Servlet implementation class Retornajson
 */
public class Retornajson extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Retornajson() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClienteController cc = new ClienteController();
		Gson gson = new Gson();
		String aux = "";
		try {
			aux = gson.toJson(cc.retornaClientes());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("json", aux);
		RequestDispatcher rd = request.getRequestDispatcher("/load.html");
		rd.forward(request, response);
	}



}
