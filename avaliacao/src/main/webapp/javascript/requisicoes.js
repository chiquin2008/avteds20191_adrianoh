function reqServlet(json, url, callback) {

	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			callback(this.responseText);
		}
	};

	xhttp.open("POST", url, true);
	xhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');

	if (json != null) {
		xhttp.send(JSON.stringify(json));
	} else {
		xhttp.send();
	}

}

var Cliente = {
	addCliente : function(e) {
		e.preventDefault();

		var t = {};
		t.nome = document.getElementById("nome").value;
		t.cpf = document.getElementById("cpf").value;
		t.logradouro = document.getElementById("logradouro").value;
		t.numero = document.getElementById("numero").value;
		t.cidade = document.getElementById("cidade").value;
		t.estado = document.getElementById("estado").value;
		t.cep = document.getElementById("cep").value;

		reqServlet(t, "addcliente", Cliente.callbackAddCliente);

		return false;
	},

	callbackAddCliente : function(responseText) {
		var resp = JSON.parse(responseText);
		var msg = document.getElementById("msg");
		msg.style.display = "block";
		msg.innerHTML = resp.msg;
	},

	retorna : function() {

		reqServlet(null, "loadcliente", Cliente.callbackretorna);

	},
	callbackretorna : function(responseText) {
		var resp = JSON.parse(responseText);
		var data = resp.data;
		var endereco = resp.data[0]["endereco"];
		var col = [];
		var count = 0;
		for ( var id in data[0]) {
			if (count < 2) {
				if (col.indexOf(id) === -1) {
					col.push(id);
				}
			}
			count++;
		}
		for ( var id in data[0]["endereco"]) {
			if (col.indexOf(id) === -1) {
				col.push(id);
			}
		}

		var tabela = document.createElement("table");
		var tr = tabela.insertRow(-1);
		for (var i = 0; i < col.length; i++) {
			var th = document.createElement("th");
			th.innerHTML = col[i];
			tr.appendChild(th);
		}
		for (var i = 0; i < data.length; i++) {

			tr = tabela.insertRow(-1);

			for (var j = 0; j < 2; j++) {
				var tabCell = tr.insertCell(-1);
				tabCell.innerHTML = data[i][col[j]];
				x = j;
			}
			for (var j = 2; j < col.length; j++) {
				var tabCell = tr.insertCell(-1);
				tabCell.innerHTML = data[i]["endereco"][col[j]];

			}
		}
		var divContainer = document.getElementById("resposta");
		divContainer.innerHTML = "";
		divContainer.appendChild(tabela);

	}
}